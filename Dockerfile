FROM debian:bookworm-slim

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends python3 && rm -rf /var/lib/apt/lists

ADD cheaprandom.py datatest.sh /

CMD ["/datatest.sh", "/mnt/datatest", "16", "1058576"]
