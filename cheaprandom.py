#!/usr/bin/python3
import random
import sys

nbytes = int(sys.argv[1])
sys.stdout.buffer.raw.write(random.randbytes(nbytes))
