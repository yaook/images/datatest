#!/bin/bash
set -euo pipefail
dir="$1"
filecount="$2"
filesize="$3"

mkdir -p "$dir/stable" "$dir/evolving"

function log() {
  pattern="%s: $1\n"
  shift
  printf "$pattern" "$(date -Iseconds)" "$@"
}

function writefiles() {
  dest="$1"
  log 'writefiles: %s' "$dest"
  for i in $(seq 1 "$filecount"); do
    python3 ./cheaprandom.py "$filesize" > "$dest/$i.blob"
  done
  sha256sum "$dest/"*".blob" > "$dest/SHA256SUM"
}

function verify() {
  dest="$1"
  log 'verify: %s' "$dest"
  sha256sum --quiet -c "$dest/SHA256SUM"
}

writefiles "$dir/stable"

while true; do
  writefiles "$dir/evolving"
  log 'sleep: 30'
  sleep 30
  verify "$dir/stable"
  verify "$dir/evolving"
done
