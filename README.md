# Datatest

A simple container which, in a loop, writes and verifies data on the
filesystem.

The default invocation will write 16 files with 1 MB each in /mnt.
